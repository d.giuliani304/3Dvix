#ifndef CONTEXT_H
#define CONTEXT_H
#include "../WindowManager/wm_globals.h"
#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>
SDL_GLContext ctx;

bool createGLContext(SDL_Window* win);
bool updateRendering(SDL_Window* win, SDL_GLContext ctx);
/* static GLuint create_Buffer(GLenum target, const void *buffer_data, GLsizei buffer_size);*/

#endif
