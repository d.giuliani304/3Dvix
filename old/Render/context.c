#include "context.h"

bool createGLContext(SDL_Window* win){
	ctx = SDL_GL_CreateContext(win);
	if (ctx==NULL){
		printf("Something went wrong with OpenGL: %s\n", SDL_GetError());
		return false;
	}else{
		glClearColor(0.3,0,0,1);
		glClear(GL_COLOR_BUFFER_BIT);
		draw3DViewport();
		drawFooterPanel();
		SDL_GL_SwapWindow(win);

		return true;
	}
}

bool updateRendering(SDL_Window* win, SDL_GLContext ctx){
	//we are going to update the context
	SDL_GetWindowSize(win, &primaryWin.x, &primaryWin.y);
	glViewport(0,0,primaryWin.x,primaryWin.y);
	glClearColor(0.021f,0.021f,0.011f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	//here starts the footer bar
	drawFooterPanel();
	draw3DViewport();
	SDL_GL_SwapWindow(win);
	return true;
}

void drawFooterPanel(){
	//set a size of the viewport that will be the size of the footer
	glViewport(0, 0, primaryWin.x, 30);
	glColor3f(0.001f*5, 0.1f*5, 0.0f);
	glRectf(-1.0f,1.0f,1.0f,0.05f);
	glColor3f(0.004f, 0.008f, 0.0f);
	glRectf(-1.0f,0.95f,1.0f,-1.0f);
}
void draw3DViewport(){
	//VAO,VBO
	//fg
	glViewport(0, 30, primaryWin.x, primaryWin.y-30);
	glColor3f(0.008f, 0.006f, 0.004f);
	glRectf(-1.0f,1.0f,1.0f,-1.0f);

}

static GLuint create_Buffer(GLenum target, const void *buffer_data, GLsizei buffer_size){
	GLuint buffer;
	glGenBuffers(1, &buffer);
	glBindBuffer(target, buffer);
	glBufferData(target, buffer_size, buffer_data, GL_STATIC_DRAW);
	return buffer;
}
