#ifndef EVENTS_MANAGER_H
#define EVENTS_MANAGER_H
#include <stdbool.h>
#include <SDL2/SDL.h>
#include "../Render/context.h"

bool RUN;
bool PAUSE;
bool RESUME;
bool checkSystem();
bool checkVideo();
bool checkAudio();
bool checkEvent();
//bool WindowEventSwitch(SDL_Event event, bool RUN);
bool mouseEvents(SDL_Event* event);
bool windowEvents(SDL_Event* event, bool RUN);
bool keyboardEvent(SDL_Event* event);
void syncrho();
bool launchCustomEvent();
#endif
