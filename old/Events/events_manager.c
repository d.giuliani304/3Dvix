#include "events_manager.h"

//****************************************************************************
//We assicure here that video, audio and event are correctly initialized
//****************************************************************************
bool checkSystem(){
	if(checkVideo() && checkAudio() && checkEvent()){
		return true;
	}else{
		SDL_Log("Error: %s",SDL_GetError());
		return false;
	}
};

bool checkVideo(){
	if (SDL_Init(SDL_INIT_VIDEO)!=0) {
		SDL_Log("Error: %s",SDL_GetError());
		return false;
	} else {
		return true;
	}
}

bool checkAudio(){
	if (SDL_Init(SDL_INIT_AUDIO)!=0) {
		SDL_Log("Error: %s",SDL_GetError());
		return false;
	} else {
		return true;
	}
}

bool checkEvent(){
	if (SDL_Init(SDL_INIT_EVENTS)!=0) {
		SDL_Log("Error: %s",SDL_GetError());
		return false;
	} else {
		return true;
	}
}
//END INIT CHECKER

//this function hendles events fired by the window, resize, close, shown etc..
//it should be here, but the methods within have to be in the WindowManager
bool windowEvents(SDL_Event* event, bool RUN){
	switch (event->window.event) {
		case SDL_WINDOWEVENT_CLOSE:
			printf("CASE CLOSE\n");
			RUN=false;
			return RUN;
		case SDL_WINDOWEVENT_SIZE_CHANGED:
			syncronizer.DELAY=100;
			updateRendering(win, ctx);
			return RUN;
	};
	return true;
}
bool keyboardEvent(SDL_Event* event){
	switch (event->key.keysym.sym) {
		case SDLK_k:
			printf("\tK was Pressed\n");
			PAUSE=true;
			return true;
		case SDLK_l:
			printf("PausaFinita\n");
			PAUSE=false;
			return false;
		default:
			return false;
	}
};
//We should move this function inside another file "inputManger???"
//other notes: when we want fire some events based on things showned inside the
//window we should handle them out of here. This should be used for only one
//purpose, return the position and button events like: CLICKED RELEASED WHEEL.
//here we'll change also the cursor style based on certain condition during
//lifecycle of the program.
bool mouseEvents(SDL_Event* event){
	if (event->type==SDL_MOUSEMOTION){
		storeMousePosition(event->motion.x, event->motion.y);
		//printf("%d %d\n", MouseStruct.xpos, MouseStruct.ypos );
		SDL_GetWindowSize(win, &primaryWin.x, &primaryWin.y);
		//CONTROL Variables moved from wm_globals to mouse_manager
		primaryWin.mx = (float) 2.0f * event->motion.x / primaryWin.x - 1;
		primaryWin.my = (float) 2.0f * event->motion.y / primaryWin.y - 1;
		primaryWin.mx_px=event->motion.x;
		primaryWin.my_px=event->motion.y;

		//Check for intersection between views, and change the cursor type
		if (event->motion.y<(primaryWin.y-panel.height+3)&&event->motion.y>(primaryWin.y-panel.height-3)){
			cursor = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZENS);
			SDL_SetCursor(cursor);
		}else{
			SDL_FreeCursor(cursor);
		}
		printf("%d x %d\n", primaryWin.mx_px, primaryWin.my_px);
		return true;
	}else{
		return false;
	}
}

void syncrho(){
	if (syncronizer.now - syncronizer.lastTick>1){
        // syncronizer.lastTick=syncronizer.now;
		printf("now - last %d\t%d\n",syncronizer.now, syncronizer.lastTick );
        syncronizer.now=SDL_GetTicks();

	}else{
		SDL_Delay(syncronizer.lastTick-syncronizer.now);
		unsigned long int x=syncronizer.now-syncronizer.lastTick;
		unsigned long int y=syncronizer.DELAY+((syncronizer.now-syncronizer.lastTick)*(1000));
		printf("X\t%li Y\t%li\n",x, y);
	}
}
bool launchCustomEvent(){
	if(!eventello.user.data2){
		printf("%u\n", eventello.user.type);
		Uint32 myEventType = SDL_RegisterEvents(1);
		printf("%p EVENTTYPE\n", &myEventType);
		if (myEventType != ((Uint32)-1)) {
			SDL_memset(&eventello, 0, sizeof(eventello)); /* or SDL_zero(eventello) */
			eventello.type = myEventType;

			eventello.user.data2 = &syncronizer.eventello;

		}
	}
	return true;

}
/*
bool WindowEventSwitch(const SDL_Event * event, bool RUN)
{
	if (event->type == SDL_WINDOWEVENT) {
		switch (event->window.event) {
		case SDL_WINDOWEVENT_SHOWN:
			SDL_Log("Window %d shown", event->window.windowID);
			return true;
		case SDL_WINDOWEVENT_HIDDEN:
			SDL_Log("Window %d hidden", event->window.windowID);
			return true;
		case SDL_WINDOWEVENT_EXPOSED:
			SDL_Log("Window %d exposed", event->window.windowID);
			return true;
		case SDL_WINDOWEVENT_MOVED:
			SDL_Log("Window %d moved to %d,%d",
					event->window.windowID, event->window.data1,
					event->window.data2);
			return true;
		case SDL_WINDOWEVENT_RESIZED:
			SDL_Log("Window %d resized to %dx%d",
					event->window.windowID, event->window.data1,
					event->window.data2);
			return true;
		case SDL_WINDOWEVENT_SIZE_CHANGED:
			SDL_Log("Window %d size changed to %dx%d",
					event->window.windowID, event->window.data1,
					event->window.data2);
			return true;
		case SDL_WINDOWEVENT_MINIMIZED:
			SDL_Log("Window %d minimized", event->window.windowID);
			return true;
		case SDL_WINDOWEVENT_MAXIMIZED:
			SDL_Log("Window %d maximized", event->window.windowID);
			return true;
		case SDL_WINDOWEVENT_RESTORED:
			SDL_Log("Window %d restored", event->window.windowID);
			return true;
		case SDL_WINDOWEVENT_ENTER:
			SDL_Log("Mouse entered window %d",
					event->window.windowID);
			return true;
		case SDL_WINDOWEVENT_LEAVE:
			SDL_Log("Mouse left window %d", event->window.windowID);
			return true;
		case SDL_WINDOWEVENT_FOCUS_GAINED:
			SDL_Log("Window %d gained keyboard focus",
					event->window.windowID);
			return true;
		case SDL_WINDOWEVENT_FOCUS_LOST:
			SDL_Log("Window %d lost keyboard focus",
					event->window.windowID);
			return true;
		case SDL_WINDOWEVENT_CLOSE:
			SDL_Log("Window %d closed", event->window.windowID);
			RUN=false;
			return RUN;
	#if SDL_VERSION_ATLEAST(2, 0, 5)
		case SDL_WINDOWEVENT_TAKE_FOCUS:
			SDL_Log("Window %d is offered a focus", event->window.windowID);
			return true;
		case SDL_WINDOWEVENT_HIT_TEST:
			SDL_Log("Window %d has a special hit test", event->window.windowID);
			return true;
	#endif
		default:
			SDL_Log("Window %d got unknown event %d",
					event->window.windowID, event->window.event);
			return true;
		}
	}
}
*/
