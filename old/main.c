#include <stdio.h>


//*****************************************************************************
//	header that contains
//	globals data of the WindowManager "module"
//*****************************************************************************
#include "WindowManager/wm_globals.h"

//*****************************************************************************
//	header that is needed for managing events and input
//*****************************************************************************
#include "Events/events_manager.h"

//*****************************************************************************
//	could be not necessary include the rendering module here,
//	looking forward for a better solution
//*****************************************************************************
#include "Render/context.h"

//*****************************************************************************
//	From here we should write the main procedures that will launch our program,
//	check the system, create our window and gameloop and then kill it.
//	we'll not pass any arguments to main function
//*****************************************************************************
int main(){
	printf("SIZE of primaryWin Struct %zu\n",sizeof(primaryWin) );
	GLint maj_ver, min_ver, num_ext;
	checkSystem();
	createMainWindow();
	createGLContext(win);
    glGetIntegerv(GL_NUM_EXTENSIONS, &num_ext);
    glGetIntegerv(GL_MAJOR_VERSION, &maj_ver);
    glGetIntegerv(GL_MINOR_VERSION, &min_ver);
    printf("%d.%d\n", maj_ver, min_ver);
	// for(int i=0;i<num_ext;i++){
	// 	printf("%s\n",glGetStringi(GL_EXTENSIONS,i));
	// }
    updateRendering(win,ctx);
	RUN=true;
	PAUSE=false;
	RESUME=false;
	// bool STARTED=true;
	//this should be used as an event owned by module "Events"
	//that returns no errors after init VIDEO AUDIO AND EVENTS by SDL2
	//Create a main loop here than move to a proper location
	SDL_Event event;
	SDL_Event pauseEvents;
	int counter=0;
	while ((RUN)){
		// syncronizer.startTick=SDL_GetTicks();
		//printf("STARTTICK%d\n",syncronizer.startTick );
		// syncronizer.DELAY=5;
		while ((SDL_PollEvent(&event)) ) {
			PAUSE=false;
			if(!windowEvents(&event, RUN)){
				RUN=false;
				break;
			}
			if(mouseEvents(&event) ){
			}
			if ((counter>=0)){
				if (keyboardEvent(&event)) {
					counter++;
					RESUME=false;
					printf("counter %d \n",counter );
				}
			}
			//printf("WHILE 2\n");
		}
		while (PAUSE) { //WHILE PAUSED
			while((!RESUME)&&(SDL_WaitEvent(&pauseEvents))) {
				if(!windowEvents(&pauseEvents, RUN)){
					printf("PRINTAAAA\n");
					PAUSE=false;
					break;
				}
				if(mouseEvents(&pauseEvents) ){
					}
				switch (pauseEvents.key.keysym.sym) {
					case SDLK_k:
						if (counter>1){
							launchCustomEvent();
							printf("event.type %u\nevent.data %p\n",eventello.user.type, eventello.user.data2 );
							//printf("sym1 %d\t sym2 %d \n",event.key.keysym.sym, pauseEvents.key.keysym.sym );
							counter =0;
						}else{
							printf("CIAO\n" );
							RESUME=true;
							PAUSE=false;
							break;

						}
				}
			}
				//printf("WHILE 4\n");

			//printf("WHILE 3\n");
		}//WHILE PAUSED
		/*events_manager*/
		// syncrho();
		//printf("WHILE 1\n");
		// syncronizer.now=SDL_GetTicks();
		// syncronizer.DIFFERENCE=(syncronizer.now-syncronizer.startTick);
		// if (syncronizer.DIFFERENCE>5){
		// 	printf("ms needed for one syncronizer.DIFFERENCE %u\n",syncronizer.DIFFERENCE );
		// }
	}
	//printf("SystemResumed\n" );
	/*while( (RUN) && (SDL_WaitEvent(&event)) ){
		if(!windowEvents(&event, RUN)){
			break;
		}if(mouseEvents(&event)){
			continue;
		}
		if (keyboardEvent(&event)) {
			printf("SystemPaused\n");
			SDL_Event pauseEvents;
			while ( (PAUSE) && (SDL_WaitEvent(&pauseEvents)) ){
				printf("SystemPaused\n");
				if(PAUSE=keyboardEvent(&pauseEvents)){
					printf("System going to be resumed\n");
					keyboardEvent(&pauseEvents);
					break;
				};
			}
		}
		printf("SystemResumed\n" );
	};
	*/
	SDL_GL_DeleteContext(ctx);
	SDL_DestroyWindow(win);
	SDL_Quit();
	return 0;
}
