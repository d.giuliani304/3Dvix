#ifndef MEM_POOL_H
#define MEM_POOL_H
//define a mathod that implement a memory pool system, with garbage collection
//Expand or shrink this pool will be usefull for avoid overflow problem.
struct ctx_Class{
	int *ID;
	int *CHILD;
};
typedef struct ctx_Class ctx_Class;

ctx_Class createContext(ctx_Class *ctx_to_create){
	return (ctx_Class)*ctx_to_create;
};
ctx_Class newContext(ctx_Class *new_CTX){
	return createContext(new_CTX);
}
struct CTX_Obj{
	ctx_Class (*NEW)();
};
typedef struct CTX_Obj CTX_Obj;
#endif
