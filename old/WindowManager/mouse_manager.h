#ifndef MOUSE_MANAGER_H
#define MOUSE_MANAGER_H
//Create a Struct that will be updated on every mouse events
//-xpos, ypos=current mouse position in pixel window
struct MOUSE_STRUCT{
	int xpos;
	int ypos;
}MouseStruct;

struct MOUSE_STRUCT storeMousePosition(int x, int y);
#endif
