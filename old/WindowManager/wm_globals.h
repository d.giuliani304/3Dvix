#ifndef WM_GLOBALS_H
#define WM_GLOBALS_H
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL.h>
#include <stdbool.h>
#include "mouse_manager.h"
struct {
	float mx;
	float my;
	int x;
	int y;
	unsigned int mx_px;
	unsigned int my_px;

}primaryWin;

struct {
	int *ID;
	int height;
	int width;
	int *ctxParent;
	float posX;
	float posY;
}panel;

typedef struct {
	struct panel* nElem[4];
	bool update;

}k_layout;

struct{
	unsigned int now;
	unsigned int lastTick;
	unsigned int startTick;
	unsigned int DIFFERENCE;
	int eventello;
	int DELAY;
}syncronizer;

SDL_Window* win;
SDL_Cursor* cursor;
SDL_Event eventello;
bool createMainWindow();
void drawFooterPanel();
void draw3DViewport();
bool firstNeighborSelect(k_layout subject);
#endif
