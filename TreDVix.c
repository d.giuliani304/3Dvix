#include "core/TredVixStructure.h"

// int omp_get_thread_num();
int omp_get_max_threads();
int omp_get_thread_num();



int main(int argc, char const *argv[]) {

		// tdvixGetSpace(SPACE_CLASS, ULLONG_MAX );
	printf("Thread rank: %d\n", omp_get_max_threads());

	SPACE_CLASS spazio={
		123456789,
		1,
		PAUSE,
		"MAIN_1000",
		&spazio
	};

	SPACE_CLASS *space[omp_get_max_threads()];

	printf("d - %llu\tl - %llu\ns - %d\tn - %s\np - %p\n",
			spazio.dimension,
			spazio.levels,
			spazio.state,
			spazio.name,
			spazio.pointer
	);
	SDL_Init(SDL_INIT_VIDEO);
	#pragma omp parallel shared(space, spazio)
	{
		printf("%d\n", omp_get_thread_num() );
		space[omp_get_thread_num()]=newSpace(space[omp_get_thread_num()], spazio, omp_get_thread_num());
		// printf("TRHEAD NUM = %d\t",omp_get_thread_num());
		// printf("POINTER OF SPACE_CLASS[%d] = %p\n",omp_get_thread_num() ,&space[omp_get_thread_num()] );
		// delSpace(space[omp_get_thread_num()]);
		printf("SPACE\n" );
		printf("POINTER OF SPACE_CLASS[%d] = %p\n",omp_get_thread_num() , space[omp_get_thread_num()] );
		printf("POINTER OF SPACE_CLASS[%d].pointer = %p\n",omp_get_thread_num() , space[omp_get_thread_num()]->pointer );
		printf("POINTER OF SPACE_CLASS[%d].name = %s\n",omp_get_thread_num() , space[omp_get_thread_num()]->name );
	}
	printf("END\n\n" );


	for (int i = 0; i < omp_get_max_threads(); i++) {
		printf("POINTER OF SPACE_CLASS[%d] = %p\n",i , space[i] );
		printf("POINTER OF SPACE_CLASS[%d].pointer = %p\n",i , space[i]->pointer );
		printf("POINTER OF SPACE_CLASS[%d].name = %s\n",i , space[i]->name );
		space[i]->window = SDL_CreateWindow(
			"space[i]->name",                  // window title
			SDL_WINDOWPOS_UNDEFINED,           // initial x position
			SDL_WINDOWPOS_UNDEFINED,           // initial y position
			250,                               // width, in pixels
			250,                               // height, in pixels
			SDL_WINDOW_RESIZABLE
		);
		space[i]->glcontext = SDL_GL_CreateContext(space[i]->window);
		SDL_Delay(1000);
		// free(space[i]->pointer);
		// printf("POINTER OF SPACE_CLASS[%d] = %p\n",i , &space[i] );
	}

	//END of generation

	// statm_t stat;
	// read_off_memory_status(stat);
	// printf("%ld\n", stat.size);

	SDL_Delay(3000);
	for (int i = 0; i < omp_get_thread_num(); i++) {
		SDL_GL_DeleteContext(space[i]->glcontext);
		SDL_DestroyWindow(space[i]->window);
		delSpace(space[i]->pointer);
	}
	SDL_Quit();
	// 	space.pointer=&space;
	// 	printf("MAX VALUE OF ULL:%llu\n",ULLONG_MAX );
	// 	printf("RUN %d\nPAUSE %d\nRESUME %d\nQUIT %d\n", RUN, PAUSE, RESUME, QUIT);
	// 	tdvixGetSpace(&space);
	return 0;
}
