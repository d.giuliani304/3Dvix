OBJS = TreDVix.o TredVixStructure.o
SOURCE = TreDVix.c core/TredVixStructure.c
OUT = 3DVIX
CC = gcc
FLAGS = -g -c -Wall -Werror
LFLAGS = -fopenmp -lSDL2
all:$(OBJS)
	$(CC) -g $(OBJS) -o $(OUT) $(LFLAGS)
TreDVix.o: TreDVix.c core/TredVixStructure.h
	$(CC) $(FLAGS) TreDVix.c $(LFLAGS)
TredVixStructure.o: core/TredVixStructure.c core/TredVixStructure.h
	$(CC) $(FLAGS) core/TredVixStructure.c
clean:
	rm -f $(OBJS) #$(OUT)
run:
	make clean | ./3DVIX
