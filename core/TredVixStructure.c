#include "TredVixStructure.h"

int x=0;
int tdvixGetSpace(SPACE_CLASS* SPACE){
	// initialize(SPACE, matrice);
	return 0;
};


int tdvixSetSpace(SPACE_CLASS* SPACE, SPACE_CLASS matrice, int num_thread) {

	char tmp[10]={"MAIN_000"};
	char ctx_name[10];
	sprintf(ctx_name, "%s%d", tmp, num_thread);
	strcpy(SPACE->name, ctx_name);
	x++;
	return 1;
	//salva in una costante l'indirizzo dello spazio

}

int initialize(SPACE_CLASS* SPACE, SPACE_CLASS matrice, int num_thread){

	// printf("%s\n",SPACE->name );

	tdvixSetSpace(SPACE, matrice, num_thread);
	// printf("%s\n",SPACE->name );
	// tdvixSetSpace(SPACE, matrice, num_thread);
	// printf("%s\n",SPACE->name );

	return 0;
}

SPACE_CLASS* newSpace(SPACE_CLASS *retval, SPACE_CLASS matrice, int num_thread) {

	// Try to allocate SPACE_CLASS structure.
	retval=(SPACE_CLASS*)calloc(1, sizeof (matrice));

	printf("Created NEW SPACE - %p\ntnum - %d\n", &retval, num_thread);
	initialize(retval, matrice, num_thread);

	// Try to allocate SPACE_CLASS data, free structure if fail.

	retval->pointer=retval;
	printf("%p\n",retval );

	return retval->pointer;
}

int delSpace (SPACE_CLASS *space1) {
	// Can safely assume space is NULL or fully built.

	free (space1);
	return 0;
}

void read_off_memory_status(statm_t result)
{
  // unsigned long dummy;
  const char* statm_path = "/proc/self/statm";

  FILE *f = fopen(statm_path,"r");
  if(!f){
    perror(statm_path);
    abort();
  }
  if(7 != fscanf(f,"%ld %ld %ld %ld %ld %ld %ld",
    &result.size,&result.resident,&result.share,&result.text,&result.lib,&result.data,&result.dt))
  {
    perror(statm_path);
    abort();
  }
  fclose(f);
}
