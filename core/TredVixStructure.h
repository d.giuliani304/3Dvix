#ifndef TDV_SPACE
#define TDV_SPACE //value
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
// #include <limits.h>
#include <SDL2/SDL.h>

//platform specific max value

typedef unsigned long long expand;

struct space_S{
	expand dimension;
	expand levels;
	unsigned char state;
	char name[10];
	struct space_S *pointer;
	SDL_Window *window;
	SDL_GLContext glcontext;
};

typedef struct space_S SPACE_CLASS;

struct window_S{
	char name[10];

};


struct {
	double x;
} actor;

typedef struct {
    unsigned long size,resident,share,text,lib,data,dt;
} statm_t;

#ifndef SYMBOL_STATE
#define SYMBOL_STATE

#define RUN 	0b0001
#define PAUSE 	0b0010
#define RESUME 	0b0011
#define QUIT 	0b0100

#endif
int tdvixGetSpace(SPACE_CLASS* space);
int tdvixSetSpace(SPACE_CLASS* space, SPACE_CLASS matrice, int num_thread);
int initialize(SPACE_CLASS* space, SPACE_CLASS matrice, int num_thread);
SPACE_CLASS* newSpace(SPACE_CLASS* space, SPACE_CLASS matrice, int num_thread);
int delSpace(SPACE_CLASS* space);
void read_off_memory_status(statm_t result);
#endif
